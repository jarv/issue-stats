#!/usr/bin/env bash
private_token=$1
pages=$(curl -s -I "https://gitlab.com/api/v4/projects/gitlab-com%2Fmigration/issues?private_token=$private_token&labels=Premigration" | perl -ne 'm/X-Total-Pages: (\d+)/ && print $1')
dir=$(mktemp -d)
echo "Using tempdir $dir"
echo "Fetching $pages pages"
for i in $(seq 1 "$pages"); do
    echo "page $i"
    curl -s -q "https://gitlab.com/api/v4/projects/gitlab-com%2Fmigration/issues?private_token=$private_token&labels=Premigration&page=$i" > $dir/issues-page-$i
done
echo "To 5 assignees for Premigration labeled issues in the GCP migration project:"
cat $dir/issues-page-* | jq -rs 'flatten | .[] | .assignees[] | .name' | sort | uniq -c | sort -rn | head -5
echo "Top 5 closed_by for all Premigration labeled issues in the GCP migration project:"
cat $dir/issues-page-* | jq -rs 'flatten | .[] | .closed_by.name' | sort | uniq -c | sort -rn | head -5
echo "Top 5 author for all Premigration labeled issues in the GCP migration project:"
cat $dir/issues-page-* | jq -rs 'flatten | .[] | .author.name' | sort | uniq -c | sort -rn | head -5
rm -rf "$dir"
